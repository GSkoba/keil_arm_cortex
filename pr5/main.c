#include "stm32f4xx.h"             // Device header
#define Red_On() GPIO_SetBits(GPIOD,GPIO_Pin_14)
#define Red_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_14)
#define Blue_On() GPIO_SetBits(GPIOD,GPIO_Pin_15)
#define Blue_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_15)
#define Green_On() GPIO_SetBits(GPIOD,GPIO_Pin_12)
#define Green_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_12)
#define Orange_On() GPIO_SetBits(GPIOD,GPIO_Pin_13)
#define Orange_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_13)
#define PAUSE 500
#define Red_Mode 0
#define Blue_Mode 1
#define Green_Mode 2
#define Orange_Mode 3
#define Button_Read() GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)


uint16_t delay_time = 0;

void SysTick_Handler (void)
{
	if (delay_time > 0)
	{
		delay_time--;
	}
}

void delay_mS( uint16_t delay_temp)
{
	delay_time = delay_temp;
	while(delay_time)
	{ }
}

void LEDs_Initialization(void) 
{
		GPIO_InitTypeDef GPIO_Init_Led;
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
		GPIO_Init(GPIOD, &GPIO_Init_Led);
}

void Button_Initialization(void) 
{
	GPIO_InitTypeDef GPIO_Init_Button;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_Init_Button.GPIO_Pin=GPIO_Pin_0;	
	GPIO_Init_Button.GPIO_Mode = GPIO_Mode_IN;
	GPIO_Init_Button.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init_Button.GPIO_OType = GPIO_OType_PP;
	GPIO_Init_Button.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_Init_Button);
}


uint8_t Mode;

int main(void)
{
	//uint32_t i;
	LEDs_Initialization();
	//GPIO_InitTypeDef GPIO_Init_Led;
	//RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	//SysTick_Config (SystemCoreClock/1000);
	/*GPIO_Init_Led.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
	GPIO_Init_Led.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init_Led.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init_Led.GPIO_OType = GPIO_OType_PP;
	GPIO_Init_Led.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_Init_Led);*/
	while(1)
	{ 
		
		switch (Mode) {
			case Red_Mode:	Red_On();delay_mS (PAUSE);	Red_Off(); break;//Mode=Blue_Mode;
			case Blue_Mode: Blue_On();delay_mS (PAUSE);Blue_Off(); break;//Mode = Green_Mode;
			case Green_Mode: Green_On();delay_mS(PAUSE);Green_Off(); break;// Mode=Orange_Mode;
			case Orange_Mode: Orange_On();delay_mS(PAUSE);Orange_Off(); break;
			default: Mode=Red_Mode; break;
			}
		if(Button_Read() == 1) 
		{
			Mode++;
		}
//		Red_On();
// //   GPIO_SetBits(GPIOD, GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15);	
//		//for (i=0;i<2500000; i++){};
//		delay_mS (PAUSE);
//		Red_Off();
//		Blue_On();
//		//GPIO_ResetBits(GPIOD, GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15);	
//		//for (i=0;i<2500000; i++){};
//		delay_mS (PAUSE);
//		Blue_Off();
//		
//		Green_On();
//		delay_mS(PAUSE);
//		Green_Off();
//		
//		Orange_On();
//		delay_mS(PAUSE);
//		Orange_Off();
	}
//	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
//  GPIOD->MODER = 0x55000000;
//	GPIOD->OTYPER = 0;
//	GPIOD->OSPEEDR = 0;
//	while(1)
//	{
//		GPIOD->ODR = 0xF000; 
//		for (i=0;i<2500000; i++){};
//		GPIOD->ODR = 0; 
//		for (i=0;i<2500000; i++){};
//  
//	}
	return 0;
}
