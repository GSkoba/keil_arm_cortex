#include "stm32f4xx.h"             // Device header
//macro
#define Red_On() GPIO_SetBits(GPIOD,GPIO_Pin_15) 				//Macro to turn on the red light bulb and put the "1" on the PD14 
#define Red_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_15) 		//Macro to turn off the red light bulb and put the "0" on the PD14 
#define Green_On() GPIO_SetBits(GPIOD,GPIO_Pin_14) 			//Macro to turn on the green light bulb and put the "1" on the PD12 
#define Green_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_14)		//Macro to turn off the green light bulb and put the "0" on the PD12 
#define PAUSE 500 //Pause value 
#define Red_Mode 0 
#define Blue_Mode 1
#define Green_Mode 2
#define Orange_Mode 3
//#define Button_Read() GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) //Macro reading the status of the selected port
#define SB1_Read() GPIO_ReadInputDataBit (GPIOA, GPIO_Pin_0)
#define SB2_Read() GPIO_ReadInputDataBit (GPIOA, GPIO_Pin_1)
#define HALL_Read() GPIO_ReadInputDataBit (GPIOB, GPIO_Pin_1)
#define START 1
#define STOP 0
#define FORWARD 0
#define REVERSE 1
#define VT1_On() GPIO_SetBits(GPIOD,GPIO_Pin_11) 
#define VT1_Off()  GPIO_ResetBits(GPIOD,GPIO_Pin_11)	
#define VT2_On() GPIO_SetBits(GPIOD,GPIO_Pin_10) 
#define VT2_Off()  GPIO_ResetBits(GPIOD,GPIO_Pin_10)	

uint8_t Button_State_1 = 0;
uint8_t Button_State_2 = 0;
uint8_t Button_State_3 = 0;
uint8_t Direction_Mode;
uint8_t Power_Mode;
uint8_t Mode;						 			//Mode value 
uint16_t delay_time = 0;		 //initialization delay time
uint8_t Hall_Mode = 0;
uint8_t VT1;
uint8_t VT2;

void SysTick_Handler (void) //function that is called from the system timer interrupt handler function
{
	//Proverka Moda 
	if (SB1_Read() != Button_State_1 && Button_State_1 == STOP)
	{
		Button_State_1 = START;
	} else if (SB1_Read() != Button_State_1 && Button_State_1 == START)
	{
		Power_Mode = !Power_Mode;
		Button_State_1 = STOP;
	}
	
	if (SB2_Read() != Button_State_2 && Button_State_2 == FORWARD)
	{
		Button_State_2 = REVERSE;
	} else if (SB1_Read() != Button_State_2 && Button_State_2 == REVERSE)
	{
		Direction_Mode = !Direction_Mode;
		Button_State_2 = FORWARD;
	}
	
	if (HALL_Read() != Hall_Mode && Hall_Mode == 0)
	{
		Hall_Mode= 1;
	} else if (HALL_Read() != Hall_Mode && Hall_Mode == 1)
	{
		Hall_Mode = 0;
	}
}
void delay_mS( uint16_t delay_temp)
{
	delay_time = delay_temp; 
	while(delay_time)  //Loop for waiting 
	{ }
}
void LEDs_Initialization(void) //function for initialization led
{
		GPIO_InitTypeDef GPIO_Init_Led; //structure containing port settings
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE); //enable port D clocking
		GPIO_Init(GPIOD, &GPIO_Init_Led); //call the initialization function
}
void Button_Initialization(void) //function for initialization button
{
	GPIO_InitTypeDef GPIO_Init_Button; //structure containing port settings
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); //enable port A clocking
	GPIO_Init_Button.GPIO_Pin=GPIO_Pin_0;	//set the output number, if the button is connected, for example to 0 port, then write GPIO_Pin_0
	GPIO_Init_Button.GPIO_Mode = GPIO_Mode_IN; //the port will work as a digital input
	GPIO_Init_Button.GPIO_Speed = GPIO_Speed_2MHz; //Speed
	GPIO_Init_Button.GPIO_OType = GPIO_OType_PP; //Push pull
	//include a pull to the "ground"
	GPIO_Init_Button.GPIO_PuPd = GPIO_PuPd_NOPULL; //without a brace, the conclusion "hangs in the air"
	GPIO_Init(GPIOA, &GPIO_Init_Button); //call the initialization function
}
const uint8_t a = 0;

int main(void)
{
	Button_Initialization();
	while(1){
		if (Power_Mode == 1 ) { Red_Off();Green_On();}
			else {Green_Off();Red_On();} 
	VT1 = Power_Mode && ((!Direction_Mode && Hall_Mode) || (Direction_Mode && !Hall_Mode));
	VT2 = Power_Mode && !((!Direction_Mode && Hall_Mode) || (Direction_Mode && !Hall_Mode));
	if (VT1 == 0) 
{
		VT1_Off();
}else VT1_On();

		if (VT2 == 0)
	{
		
		VT2_Off();
	}else VT2_On();
}
	return 0;
}	