#include "stm32f4xx.h"             // Device header
//macro
#define Red_On() GPIO_SetBits(GPIOD,GPIO_Pin_14) 				//Macro to turn on the red light bulb and put the "1" on the PD14 
#define Red_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_14) 		//Macro to turn off the red light bulb and put the "0" on the PD14 
#define Blue_On() GPIO_SetBits(GPIOD,GPIO_Pin_15) 			//Macro to turn on the blue light bulb and put the "1" on the PD15 
#define Blue_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_15)		//Macro to turn off the blue light bulb and put the "1" on the PD15
#define Green_On() GPIO_SetBits(GPIOD,GPIO_Pin_12) 			//Macro to turn on the green light bulb and put the "1" on the PD12 
#define Green_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_12)		//Macro to turn off the green light bulb and put the "0" on the PD12 
#define Orange_On() GPIO_SetBits(GPIOD,GPIO_Pin_13) 		//Macro to turn on the orange light bulb and put the "1" on the PD13
#define Orange_Off() GPIO_ResetBits(GPIOD,GPIO_Pin_13)	//Macro to turn off the orange light bulb and put the "0" on the PD13 
#define PAUSE 500 //Pause value 
#define Red_Mode 0 
#define Blue_Mode 1
#define Green_Mode 2
#define Orange_Mode 3
#define Button_Read() GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) //Macro reading the status of the selected port

uint8_t Button_State = 0;

uint8_t Mode; //Mode value 

uint16_t delay_time = 0; //initialization delay time

void SysTick_Handler (void) //function that is called from the system timer interrupt handler function
{
	//Proverka Moda 
	if (Button_Read() != Button_State && Button_State == 0)
	{
		Button_State = 1;
	} else if (Button_Read() != Button_State && Button_State == 1)
	{
		Mode++;
		Button_State = 0;
	}
			
//	if (delay_time > 0)
//	{
//		delay_time--; //decrement 
//	}
}

void delay_mS( uint16_t delay_temp)
{
	delay_time = delay_temp; 
	while(delay_time)  //Loop for waiting 
	{ }
}

void LEDs_Initialization(void) //function for initialization led
{
		GPIO_InitTypeDef GPIO_Init_Led; //structure containing port settings
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE); //enable port D clocking
		GPIO_Init(GPIOD, &GPIO_Init_Led); //call the initialization function
}

void Button_Initialization(void) //function for initialization button
{
	GPIO_InitTypeDef GPIO_Init_Button; //structure containing port settings
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); //enable port A clocking
	GPIO_Init_Button.GPIO_Pin=GPIO_Pin_0;	//set the output number, if the button is connected, for example to 0 port, then write GPIO_Pin_0
	GPIO_Init_Button.GPIO_Mode = GPIO_Mode_IN; //the port will work as a digital input
	GPIO_Init_Button.GPIO_Speed = GPIO_Speed_2MHz; //Speed
	GPIO_Init_Button.GPIO_OType = GPIO_OType_PP; //Push pull
	//include a pull to the "ground"
	GPIO_Init_Button.GPIO_PuPd = GPIO_PuPd_NOPULL; //without a brace, the conclusion "hangs in the air"
	GPIO_Init(GPIOA, &GPIO_Init_Button); //call the initialization function
}


const uint8_t a = 0;

int main(void) 
{
	
	LEDs_Initialization();//call the initialization function LEDs
	while(1)
	{ 
		//Odno nagatie odin regim 
		//if(Button_Read() == 1) //if button press then light  bulb change 
		{
		switch (Mode%4 ) {
			//call function 
			case Red_Mode:	Orange_Off(); Red_On(); /*delay_mS (PAUSE)  Mode=Blue_Mode;*/ break; 
			case Blue_Mode: Red_Off(); Blue_On();/*delay_mS (PAUSE); Mode = Green_Mode;*/ break;
			case Green_Mode: Blue_Off(); Green_On();/*delay_mS(PAUSE); Mode=Orange_Mode;*/ break;
			case Orange_Mode: Green_Off(); Orange_On();/*delay_mS(PAUSE)*/; /*Mode=Red_Mode;*/ break;
			//default: GPIO_ResetBits(GPIOD,GPIO_Pin_15,); break;
			}
		}
	}
	return 0;
}
